import json
import logging
import os


def key_strings_to_int(d):
    return dict((int(k), (key_strings_to_int(v) if isinstance(v, dict) else v)) for k, v in d.items())


def _getConfigPath(name='default'):
    config_path = '~/.new-jira-burn-up-and-down-{}.rc'.format(name)
    return os.path.expanduser(config_path)


def loadConfiguration(config_name):
    config_path = _getConfigPath(config_name)
    try:
        with open(config_path, 'rt') as f:
            config = json.load(f)

        logging.getLogger('configuration').info('Loaded configuration from %s', config_path)
    except (ValueError, FileNotFoundError):
        logging.getLogger('configuration').error('Could not load configuration from: %s', config_path)
        config = createDefaultConfiguration()

    return config


def saveConfiguration(config_name, config):
    config_path = _getConfigPath(config_name)
    with open(config_path, 'wt') as f:
        json.dump(config, f, indent=4)

    logging.getLogger('configuration').info('Saved configuration to %s', config_path)
    logging.getLogger('configuration').debug(json.dumps(config))


def createDefaultConfiguration():
    logging.getLogger('configuration').info('Creating default configuration')
    return json.loads('''\
        {
            "url":             "http://localhost:8080",
            "username":        "user",
            "password":        "",
            "issueQuery":      "",
            "currentBoardId":  0,
            "currentSprintId": 0,
            "availability":    0,
            "burnUpBudget":    0,
            "showWeekends":    false
        }''')


def isValidConfig(config):
    validUrl = config['url'] != ""
    validUser = config['username'] != ""
    validBoard = config['currentBoardId'] != 0
    validSprint = config['currentSprintId'] != 0

    return validUrl and validUser and validBoard and validSprint
