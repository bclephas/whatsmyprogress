import datetime
from dateutil import parser

import numpy as np
import pytz
import tzlocal


class SortUtils:
    @staticmethod
    def byTimestamp(x):
        return TimestampUtils.timestamp_to_seconds(x[0])

    @staticmethod
    def byResolutionDate(x):
        if x['fields']['resolutiondate']:
            result = TimestampUtils.timestamp_to_seconds(parser.parse(x['fields']['resolutiondate']))
        else:
            result = 0

        return result


class TimestampUtils:
    @staticmethod
    def timestamp_to_seconds(timestamp):
        epoch = datetime.datetime.fromtimestamp(0, pytz.utc)
        return (timestamp - epoch).total_seconds()

    @staticmethod
    def x_timestamps_to_seconds(x_y_data):
        return [[TimestampUtils.timestamp_to_seconds(x), y] for x, y in x_y_data]

    @staticmethod
    def x_timestamps_to_seconds_np(x_y_data):
        return np.array([[TimestampUtils.timestamp_to_seconds(x), y] for x, y in x_y_data])

    @staticmethod
    def timestamp_to_jqltimestamp(ts):
        localzone = tzlocal.get_localzone()
        if str(ts.tzinfo) != str(localzone):
            raise RuntimeError('Timezone of timestamp (%s) is not equal to local timezone (%s)' % (repr(ts.tzinfo), repr(localzone)))
        return ts.strftime('"%Y-%m-%d %H:%M"')

    @staticmethod
    def parseBurndownTimestamp(ts):
        localzone = tzlocal.get_localzone()
        naive = datetime.datetime.fromtimestamp(int(ts) / 1000, tz=pytz.utc).replace(tzinfo=None)
        return localzone.localize(naive)
