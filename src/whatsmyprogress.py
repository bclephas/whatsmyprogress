#!/usr/bin/env python

import argparse
import copy
import datetime
from dateutil import parser
import logging
import os
import platform
import textwrap
import time
import sys

import numpy as np
import pytz
import requests
import tzlocal

from jiraconnector import Jira7Instance, JiraRestConnector
import connectiondialog
import configuregraphdialog
import model
from utils import TimestampUtils
import configuration

from PyQt5.QtCore import QObject, Qt, QTimer, QRectF
from PyQt5.QtWidgets import qApp, QAction, QApplication, QDialog, QFileDialog, QLabel, QMainWindow, QMessageBox, QStyle, QWidget
from PyQt5.QtGui import QIcon, QImageWriter

import PyQt5  # import PyQt5 explicitly before pyqtgraph to stop it from using PyQt4
import pyqtgraph as pg


# TODO: TODOs
# - save history for past sprints (availability, burnupbudget, ...); very nice to have
# - option to configure models refresh rate, and showing annotations


class GUI(QMainWindow):
    def __init__(self, model, args):
        super().__init__()
        self.log = logging.getLogger('GUI')

        self.model = model
        self.args = args
        self.model.dataChanged.connect(self.updateChart)

        self.setWindowTitle("What's my progress?")

        self.chartView = self._createChartView()
        self.setCentralWidget(self.chartView)

        self.statusBar().showMessage('')

        self.statusMessage = QLabel()
        self.statusMessage.setText("Requesting data. Please wait ...")
        self.statusBar().addPermanentWidget(self.statusMessage)

        self._createMenu()

    def _createMenu(self):
        # For icons: https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
        # Ensure that on the Windows platform the correct icons are used
        # The icons are a snapshot of the KDE Oxygen theme
        if platform.system() == 'Windows':
            QIcon.setThemeSearchPaths(QIcon.themeSearchPaths() + [os.path.join(os.path.abspath(os.path.dirname(__file__)), "custom-icons")])
            QIcon.setThemeName("windows")
        self.log.info("Setting icon theme search paths to: %s", QIcon.themeSearchPaths())

        menuBar = self.menuBar()

        # TODO: change document-properties icon to something more correct
        configureConnectionIcon = QIcon.fromTheme("document-properties")
        configureConnectionAction = QAction(configureConnectionIcon, '&Configure Connection...', self)
        configureConnectionAction.triggered.connect(self.openConnectionDialog)

        refreshBoardsAndSprintsIcon = QIcon.fromTheme("view-refresh")
        refreshBoardsAndSprintsAction = QAction(refreshBoardsAndSprintsIcon, '&Refresh Boards and Sprints', self)
        refreshBoardsAndSprintsAction.triggered.connect(self.updateBoardsAndSprints)

        saveConfigIcon = QIcon.fromTheme("document-save")
        saveConfigAction = QAction(saveConfigIcon, '&Save Connection Settings', self)
        saveConfigAction.triggered.connect(self.saveConnectionConfiguration)

        closeIcon = QIcon.fromTheme("application-exit")
        exitAction = QAction(closeIcon, '&Quit', self)
        exitAction.setShortcut('CTRL+Q')
        exitAction.setMenuRole(QAction.QuitRole)
        exitAction.triggered.connect(qApp.quit)

        configureGraphIcon = QIcon.fromTheme("document-properties")
        configureGraphAction = QAction(configureGraphIcon, '&Configure Graph...', self)
        # Set the menu role for this configuration as that is the most often used config item
        configureGraphAction.setMenuRole(QAction.PreferencesRole)
        configureGraphAction.triggered.connect(self.openConfigureGraphDialog)

        saveGraphConfigIcon = QIcon.fromTheme("document-save")
        saveGraphConfigAction = QAction(saveConfigIcon, '&Save Graph Settings', self)
        saveGraphConfigAction.triggered.connect(self.saveGraphConfiguration)

        copyToClipboardIcon = QIcon.fromTheme('edit-copy')
        copyToClipboardAction = QAction(copyToClipboardIcon, '&Copy to Clipboard', self)
        copyToClipboardAction.setShortcut('CTRL+C')
        copyToClipboardAction.triggered.connect(self.copyGraphToClipboard)

        saveAsIcon = QIcon.fromTheme("document-save-as")
        saveAsAction = QAction(saveAsIcon, 'Save As...', self)
        saveAsAction.setShortcut('CTRL+SHIFT+S')
        saveAsAction.triggered.connect(self.saveGraphAs)

        refreshIcon = QIcon.fromTheme("view-refresh")
        refreshAction = QAction(refreshIcon, '&Refresh', self)
        refreshAction.setShortcut('F5')
        refreshAction.triggered.connect(self.requestModelUpdate)

        aboutBoxIcon = QIcon.fromTheme("help-about")
        aboutBoxAction = QAction(aboutBoxIcon, 'About Whatsmyprogress', self)
        aboutBoxAction.setMenuRole(QAction.AboutRole)
        aboutBoxAction.triggered.connect(self.showAboutBox)

        aboutQtIcon = QIcon.fromTheme("")
        aboutQtAction = QAction(aboutQtIcon, 'About Qt', self)
        aboutQtAction.setMenuRole(QAction.AboutQtRole)
        aboutQtAction.triggered.connect(qApp.aboutQt)

        fileMenu = menuBar.addMenu('&File')
        fileMenu.addAction(exitAction)

        connectionMenu = menuBar.addMenu('&Connection')
        connectionMenu.addAction(configureConnectionAction)
        connectionMenu.addAction(saveConfigAction)
        connectionMenu.addSeparator()
        connectionMenu.addAction(refreshBoardsAndSprintsAction)

        graphMenu = menuBar.addMenu('&Graph')
        graphMenu.addAction(configureGraphAction)
        graphMenu.addAction(saveGraphConfigAction)
        graphMenu.addSeparator()
        graphMenu.addAction(refreshAction)
        graphMenu.addSeparator()
        graphMenu.addAction(copyToClipboardAction)
        graphMenu.addAction(saveAsAction)

        helpMenu = menuBar.addMenu('&Help')
        helpMenu.addAction(aboutBoxAction)
        helpMenu.addAction(aboutQtAction)

    def requestModelUpdate(self):
        self.statusMessage.setText("Updating graph data. Please wait ...")
        self.model.update()

    def updateBoardsAndSprints(self):
        self.model.updateAvailableBoardsAndSprints()

    def openConfigureGraphDialog(self):
        cfgDlg = configuregraphdialog.ConfigureGraphDialog(self.model)
        if cfgDlg.exec_() == QDialog.Accepted:
            availability, burnUpBudget, boardId, sprintId, issueQuery, showWeekends = cfgDlg.getGraphConfiguration()
            self.model.setAvailability(availability)
            self.model.setBurnUpBudget(burnUpBudget)
            self.model.setBoardId(boardId)
            self.model.setSprintId(sprintId)
            self.model.jiraInstance.setIssueQuery(issueQuery)
            self.model.setAdjustForHiddenWeekends(not showWeekends)
            self.model.update()

    def openConnectionDialog(self):
        connector = self.model.jiraInstance.getConnector()

        connectionDialog = connectiondialog.ConnectionDialog(connector.url,
                                                             connector.authentication[0],
                                                             connector.authentication[1])

        if connectionDialog.exec_() == QDialog.Accepted:
            url, username, password = connectionDialog.getConnectionData()
            restConnector = JiraRestConnector(url, username, password)
            self.model.jiraInstance.setConnector(restConnector)
            self.log.info('Connector change: ' + restConnector.url + '. Forcing update')
            self.model.update()

    # Note: the current design only saves the relevant config part, ignoring the rest of the config.
    # e.g. graph config has changed, saving the connection settings will not remember the graph settings.
    # As it is expected that either the config or the graph settings are changed (with graph settings changing
    # more often), this design is acceptable.

    def saveConnectionConfiguration(self):
        self.log.info('Saving connection configuration')
        config = configuration.loadConfiguration(self.args.config_name)

        config['url'] = self.model.jiraInstance.getConnector().url
        config['username'] = self.model.jiraInstance.getConnector().authentication[0]
        config['password'] = ''  # Force empty password

        configuration.saveConfiguration(self.args.config_name, config)

    def saveGraphConfiguration(self):
        self.log.info('Saving graph configuration')
        config = configuration.loadConfiguration(self.args.config_name)

        config['currentBoardId'] = self.model.getBoardId()
        config['currentSprintId'] = self.model.getSprintId()
        config['issueQuery'] = self.model.jiraInstance.getIssueQuery()
        config['availability'] = self.model.getAvailability()
        config['burnUpBudget'] = self.model.getBurnUpBudget()
        config['showWeekends'] = not self.model.getAdjustForHiddenWeekends()

        configuration.saveConfiguration(self.args.config_name, config)

    def showAboutBox(self):
        QMessageBox.about(self,
                          'About Whatsmyprogress',
                          textwrap.dedent('''\
                          <p><b>Whatsmyprogress</b></p>
                          <p>Simple tool that combines a JIRA project's burndown and burnup graphs<br/>
                          into one graph.</p>
                          <p><a href="https://bitbucket.org/bclephas/whatsmyprogress">Website</a>.<br/>
                          Core graph routines based on Griffon26's <a href="https://github.com/Griffon26/jiraburnupanddown">jiraburnupanddown</a>.<br/>
                          Icons: KDE Oxygen theme, <a href="https://techbase.kde.org/Projects/Oxygen/Licensing">LGPL 3 licensed</a>.
                          </p>'''))

    def _getGraphPixmap(self):
        return self.chartView.grab()

    def copyGraphToClipboard(self):
        pixmap = self._getGraphPixmap()
        QApplication.clipboard().setPixmap(pixmap)

    def saveGraphAs(self):
        formats = ('*.%s' % bytes(fmt).decode('latin-1') for fmt in QImageWriter.supportedImageFormats())
        filename, _ = QFileDialog.getSaveFileName(None, 'Save File', '', 'Images (%s)' % ' '.join(formats))
        if filename:
            pixmap = self._getGraphPixmap()
            pixmap.save(filename)

    def _createChartView(self):
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        pg.setConfigOption('antialias', True)

        chartView = pg.PlotWidget(name='ProgressChart')

        chartView.hideButtons()
        chartView.setMenuEnabled(enableMenu=False)
        chartView.getViewBox().setMouseEnabled(x=False, y=False)
        chartView.showGrid(x=False, y=True, alpha=0.3)
        chartView.getAxis('bottom').setStyle(tickLength=0)
        chartView.getAxis('left').setStyle(tickLength=0)

        return chartView

    def updateChart(self):
        if self.model.isValid():
            self._drawChart()

            sprint = self.model.getSprintData(self.model.getBoardId(), self.model.getSprintId())

            message = '{}: Availability: {} hrs, Burn-up Budget: {} hrs'.format(
                sprint.get('name', 'Unable to get sprint name'),
                str(self.model.getAvailability()),
                str(self.model.getBurnUpBudget()))
            self.statusMessage.setText(message)
        else:
            self.statusMessage.setText('No data to show. Connection issues or incorrect filter query?')
            self.log.info('Cleared chart')
            self._drawEmptyChart()

        self.log.info('Updated chart')

    @staticmethod
    def createSegments(inputdata, connected):
        dataSet = [inputdata[0]]
        previousY = inputdata[0][1]

        for value in inputdata[1:]:
            dataSet.append([value[0], previousY])
            if not connected:
                dataSet.append(None)
            dataSet.append(value)
            previousY = value[1]

        return dataSet

    def _drawEmptyChart(self):
        self.chartView.clear()
        self.log.debug('Redrawn empty graph')

    def _drawChart(self):
        self.chartView.clear()

        ((min_x, min_y), (max_x, max_y)) = self.model.getRanges()
        self.chartView.setRange(QRectF(min_x, min_y, max_x - min_x, max_y - min_y), padding=0)

        self._drawHorizontalAxis()
        self._drawVerticalAxis()

        self._drawZeroLine()
        self._drawSprintScopeLine()

        self._drawActualBurnDownLine()
        self._drawIdealBurndownLine()
        self._drawExpectedBurndownLine()

        self._drawActualBurnupLine()
        self._drawIdealBurnupLine()
        self._drawProjectedBurnupLine()

        # Draw annotations
        currentTimestamp, currentActualBurndownValue = self.model.getBurnDownData()[-1]
        currentIdealBurndownValue = self.model.getIdealBurndownValueAtTimestamp(currentTimestamp)
        self._drawBudgetOverrun(self.model.getZeroData()[-1][0], self.model.getProjectedBurnupHeight())
        self._drawPointsBehind(currentTimestamp, currentIdealBurndownValue, currentActualBurndownValue)

        self.log.debug('Redrawn graph')

    def _drawHorizontalAxis(self):
        dayLabels = self.model.getDayLabels()

        self.chartView.getAxis('bottom').setTicks([TimestampUtils.x_timestamps_to_seconds(dayLabels)])

    def _drawVerticalAxis(self):
        dayLines = self.model.getDayLines()

        targetRect = self.chartView.getViewBox().targetRect()
        min_y = targetRect.top()
        max_y = targetRect.bottom()

        markings = [[(value[0], min_y), (value[0], max_y)] for value in dayLines]

        pen = pg.mkPen('#e0e0e0', width=1)
        for marking in markings:
            self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(marking), pen=pen)

    def _drawZeroLine(self):
        pen = pg.mkPen('k', width=1, style=Qt.DashLine)
        pen.setDashPattern([10, 10])

        sprintStart, sprintEnd = self.model.getSprintDates()
        zeroData = [[sprintStart, 0], [sprintEnd, 0]]
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(zeroData), pen=pen)

    def _drawActualBurnDownLine(self):
        pen = pg.mkPen('b', width=2)
        burnDownData = self.model.getBurnDownData()
        segments = GUI.createSegments(burnDownData, connected=True)
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(segments), pen=pen)

    def _drawIdealBurndownLine(self):
        pen = pg.mkPen('#c0c0c0', width=2)
        idealBurnDownData = self.model.getIdealBurnDownData()
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(idealBurnDownData), pen=pen)

    def _drawActualBurnupLine(self):
        pen = pg.mkPen('r', width=2)
        actualBurnupData = self.model.getBurnUpData()
        segments = GUI.createSegments(actualBurnupData, connected=True)
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(segments), pen=pen)

    def _drawIdealBurnupLine(self):
        pen = pg.mkPen('#c0c0c0', width=2)
        idealBurnupData = self.model.getIdealBurnUpData()
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(idealBurnupData), pen=pen)

    def _drawSprintScopeLine(self):
        pen = pg.mkPen('k', width=2)
        lineData = self.model.getSprintScopeLine()
        segments = GUI.createSegments(lineData, connected=True)
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(segments), pen=pen)

    def _drawProjectedBurnupLine(self):
        pen = pg.mkPen('r', width=1, style=Qt.DashLine)
        pen.setDashPattern([10, 10])
        projectedBurnupData = self.model.getProjectedBurnupData()
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(projectedBurnupData), pen=pen)

    def _drawExpectedBurndownLine(self):
        pen = pg.mkPen('#008000', width=2)
        expectedBurndownData = self.model.getExpectedBurndownData()
        self.chartView.plot(TimestampUtils.x_timestamps_to_seconds_np(expectedBurndownData), pen=pen)

    def _drawVerticalAnnotatedArrow(self, x, y1, y2, text, xanchor):
        # draw arrows in light gray so they don't look like part of the burndown when seen from a distance
        arrowColor = '#c0c0c0'
        pen = pg.mkPen(arrowColor, width=1)

        # How much space should be left between the tips of the arrows and the specified Y positions
        # This is to make it even clearer that the arrow is not part of the burndown when seen from a distance
        arrowOffset = 0.25

        arrowTop = max(y1, y2) - arrowOffset
        arrowBottom = min(y1, y2) + arrowOffset

        arrowLine = [(x, arrowTop),
                     (x, arrowBottom)]
        self.chartView.plot(np.array(arrowLine), pen=pen)

        self.chartView.addItem(pg.ArrowItem(pos=(x, arrowTop), angle=90, tipAngle=40, headLen=10, pen=None, brush=arrowColor))
        self.chartView.addItem(pg.ArrowItem(pos=(x, arrowBottom), angle=-90, tipAngle=40, headLen=10, pen=None, brush=arrowColor))

        textItem = pg.TextItem(text=text, color='k', anchor=(xanchor, 0.5))
        textItem.setPos(x, (arrowTop + arrowBottom) / 2)
        self.chartView.addItem(textItem)

    def _drawBudgetOverrun(self, max_x, projectedBurnupHeight):
        max_x = TimestampUtils.timestamp_to_seconds(max_x)

        points = round(abs(projectedBurnupHeight))
        # Show the overrun arrow when being more than 2 points over the budget
        if points >= 2:
            self._drawVerticalAnnotatedArrow(max_x, 0, projectedBurnupHeight, '%d pts' % points, 0)

    def _drawPointsBehind(self, currentTimestamp, currentIdealBurndownValue, currentActualBurndownValue):
        x = TimestampUtils.timestamp_to_seconds(currentTimestamp)

        points = round(abs(currentIdealBurndownValue - currentActualBurndownValue))
        # Show the behind arrow when being more than 2 points behind the ideal burndown value
        if points >= 2:
            self._drawVerticalAnnotatedArrow(x, currentIdealBurndownValue, currentActualBurndownValue, '%d pts' % points, 1)


def parse_command_line():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config-name',
                        default='default',
                        help='Name of the config to load. Not the path')
    parser.add_argument('--debug',
                        action='store_true',
                        default=False,
                        help='Show more debug tracing')
    return parser.parse_args()


def whatsmyprogress():
    args = parse_command_line()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    app = QApplication(sys.argv)
    app.setWindowIcon(app.style().standardIcon(getattr(QStyle, 'SP_ComputerIcon')))

    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

    # TODO: only show dialog if config is not found
    config = configuration.loadConfiguration(args.config_name)
    connectionDialog = connectiondialog.ConnectionDialog(config['url'],
                                                         config['username'],
                                                         config['password'])

    if connectionDialog.exec_() == QDialog.Accepted:
        url, username, password = connectionDialog.getConnectionData()
        config['url'] = url
        config['username'] = username
        config['password'] = password
    else:
        logging.getLogger('main').info('Quit')
        sys.exit(0)

    restConnector = JiraRestConnector(config['url'],
                                      config['username'],
                                      config['password'])
    jiraInstance = Jira7Instance(restConnector, config['issueQuery'])

    # Quick hacks to force immediate showing
    # config['currentboardid'] = 8185
    # config['currentsprintid'] = 16164     # ATTEST Stories, Sprint 99
    # config['currentsprintid'] = 16165     # ATTEST Stories, Sprint 100
    # config['availability'] = 600
    # config['burnupbudget'] = 120

    burnUpDownModel = model.Model()
    gui = GUI(burnUpDownModel, args)

    burnUpDownModel.configure(jiraInstance,
                              config['currentBoardId'],
                              config['currentSprintId'],
                              config['availability'],
                              config['burnUpBudget'])
    burnUpDownModel.setAdjustForHiddenWeekends(not config['showWeekends'])
    if configuration.isValidConfig(config):
        burnUpDownModel.updateAvailableBoardsAndSprints()
        burnUpDownModel.update()

    gui.show()
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QApplication.instance().exec_()


if __name__ == '__main__':
    try:
        whatsmyprogress()
    except Exception:
        raise
