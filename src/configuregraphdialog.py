import logging

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QCheckBox, QComboBox, QDialog, QDialogButtonBox, QGridLayout, QLabel, QLineEdit, QPlainTextEdit
from PyQt5.QtGui import QCursor, QIntValidator
from PyQt5.QtWidgets import QApplication


class ConfigureGraphDialog(QDialog):
    def __init__(self, model):
        super().__init__()
        self.log = logging.getLogger('ConfigureGraphDialog')

        self.model = model

        availability = self.model.getAvailability()
        burnUpBudget = self.model.getBurnUpBudget()
        boards = self.model.getAvailableBoards()
        showWeekends = not self.model.getAdjustForHiddenWeekends()

        self.setWindowTitle('Configure graph')

        self.boardLabel = QLabel('Scrum board')
        self.boardsComboBox = QComboBox()
        self.boardsComboBox.activated.connect(self._boardSelectionChanged)
        self.boardsComboBox.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.sprintLabel = QLabel('Sprint')
        self.sprintsComboBox = QComboBox()
        self.sprintsComboBox.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.availabilityLabel = QLabel('Availability')
        self.availabilityEdit = QLineEdit(str(availability))
        self.availabilityEdit.setValidator(QIntValidator())

        self.burnUpBudgetLabel = QLabel('burnUp Budget')
        self.burnUpBudgetEdit = QLineEdit(str(burnUpBudget))
        self.burnUpBudgetEdit.setValidator(QIntValidator())

        self.burnupIssueQueryLabel = QLabel('Issue query (JQL)')
        self.burnupIssueQueryLabel.setToolTip('<span>Work logged during the sprint on issues returned '
                                              'by this query will be plotted in the burnup</span>')

        self.showWeekendsLabel = QLabel('Show weekends')
        self.showWeekendsCheckBox = QCheckBox()
        self.showWeekendsCheckBox.setChecked(showWeekends)

        # Create a new MyPlainTextEdit class specifically to override the sizeHint:
        # - three times the height of a QTextEdit to create some space for a JQL query
        # - twice the width of the QTextEdit to create some space for URLs
        # Changing the width of this control to create room for another control
        # is a bit of a hack, but I don't feel like subclassing two controls
        # just to fix it properly.
        size = self.availabilityEdit.sizeHint()
        size.setHeight(size.height() * 3)
        size.setWidth(size.width() * 2)

        class MyPlainTextEdit(QPlainTextEdit):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)

            def sizeHint(self):
                return size

        self.burnupIssueQueryEdit = MyPlainTextEdit(self.model.getBurnupIssueQuery())
        self.queryExampleLabel = QLabel('e.g. issuetype = Task')

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.buttonBox.button(QDialogButtonBox.Ok).setText('Configure')

        self.gridLayout = QGridLayout(self)

        self.gridLayout.addWidget(self.boardLabel, 0, 0)
        self.gridLayout.addWidget(self.boardsComboBox, 0, 1)
        self.gridLayout.addWidget(self.sprintLabel, 1, 0)
        self.gridLayout.addWidget(self.sprintsComboBox, 1, 1)
        self.gridLayout.addWidget(self.availabilityLabel, 2, 0)
        self.gridLayout.addWidget(self.availabilityEdit, 2, 1)
        self.gridLayout.addWidget(self.burnUpBudgetLabel, 3, 0)
        self.gridLayout.addWidget(self.burnUpBudgetEdit, 3, 1)
        self.gridLayout.addWidget(self.burnupIssueQueryLabel, 4, 0)
        self.gridLayout.addWidget(self.burnupIssueQueryEdit, 4, 1)
        self.gridLayout.addWidget(self.queryExampleLabel, 5, 1)
        self.gridLayout.addWidget(self.showWeekendsLabel, 6, 0)
        self.gridLayout.addWidget(self.showWeekendsCheckBox, 6, 1)

        self.gridLayout.addWidget(self.buttonBox, 7, 0, 1, 2)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.updateAvailableBoards(boards)
        index = self.boardsComboBox.findData(self.model.getBoardId())
        if index >= 0:
            self.boardsComboBox.setCurrentIndex(index)

        self._boardSelectionChanged(index)
        index = self.sprintsComboBox.findData(self.model.getSprintId())
        if index >= 0:
            self.sprintsComboBox.setCurrentIndex(index)
        else:
            self.log.error('Sprint not found')

    def getGraphConfiguration(self):
        return (int(self.availabilityEdit.text()),
                int(self.burnUpBudgetEdit.text()),
                self.boardsComboBox.currentData(),
                self.sprintsComboBox.currentData(),
                self.burnupIssueQueryEdit.toPlainText(),
                self.showWeekendsCheckBox.isChecked())

    def _boardSelectionChanged(self, boardIndex):
        boardId = self.boardsComboBox.itemData(boardIndex)

        QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))

        self.model.on_update_boards_and_sprints_ready.connect(self._boardsReceived)
        self.log.debug('Requesting sprints for board %d', boardId)
        self.model.updateAvailableBoardsAndSprints(boardId)

    def _boardsReceived(self):
        boardId = self.boardsComboBox.currentData()
        self.log.debug('Update sprints for board %d', boardId)
        sprints = self.model.getAvailableSprints(boardId)
        self.updateAvailableSprints(sprints)

        QApplication.restoreOverrideCursor()

        self.model.on_update_boards_and_sprints_ready.disconnect(self._boardsReceived)

    def updateAvailableBoards(self, boards):
        self.boardsComboBox.clear()
        for boardName, boardId in sorted((boardName, boardId) for boardId, boardName in boards.items()):
            friendlyBoardName = '{} (id: {})'.format(boardName, boardId)
            self.log.debug('Adding board %s', friendlyBoardName)
            self.boardsComboBox.addItem(friendlyBoardName, boardId)

        index = self.boardsComboBox.findData(self.model.getBoardId())
        if index >= 0:
            self.boardsComboBox.setCurrentIndex(index)

    def updateAvailableSprints(self, sprints):
        self.sprintsComboBox.clear()
        for sprintId, sprintData in reversed(sorted(sprints.items())):
            friendlySprintName = '{} (id: {})'.format(sprintData['name'], sprintId)
            self.log.debug('Adding Sprint %s', friendlySprintName)
            self.sprintsComboBox.addItem(friendlySprintName, sprintId)

        index = self.sprintsComboBox.findData(self.model.getSprintId())
        if index >= 0:
            self.sprintsComboBox.setCurrentIndex(index)
