from dateutil import parser
import logging
import json
import urllib.parse

import requests
import tzlocal

from utils import TimestampUtils


class Connector:
    def __init__(self, url, username, password=''):
        self.url = url
        self.authentication = (username, password)

    def getData(self, resource, params=None):
        '''Function that returns requested data over the set connection'''
        pass

    def setData(self, resource, params=None):
        '''Function that stores the data'''
        pass

    def setConnectionData(self, url, username, password):
        '''Sets the connection details for the connection'''
        self.url = url
        self.authentication = (username, password)


class JiraRestConnector(Connector):
    def __init__(self, url, username, password):
        super().__init__(url, username, password)

    def getData(self, resource, params=None):
        log = logging.getLogger('JiraRestConnector')

        jsonData = {}
        try:
            log.info('Request data for %s %s' % (urllib.parse.urljoin(self.url, resource), params))
            r = requests.get(urllib.parse.urljoin(self.url, resource), params=params, auth=self.authentication, verify=False)
            r.raise_for_status()
            jsonData = r.json()
            log.debug('Received data (raw): %s\n', jsonData)
        except requests.HTTPError as he:
            log.error(he)
            message = self._formatFriendlyErrorMessage(he)
            log.error("HTTP error: %s", message)
        except requests.ConnectionError as ce:
            log.error("Connection error: %s", ce)

        return jsonData

    def setData(self, resource, params=None):
        super().setData(resource, params)

    def _formatFriendlyErrorMessage(self, e):
        status_code = e.response.status_code

        # This code is disabled because the Bad Request does not arrive here.
        # Instead of the other errors that are the result of failed requests performed by the model,
        # the Bad Request comes from a request done by updateChart, which is used as a slot.
        # This exception is not propagated to the code that triggers the signal, moreover
        # not handling exceptions in a slot results in undefined behaviour according to the
        # PyQt documentation. Probably all requests for data should move to the model.
        #
        if status_code == 400:  # Bad Request
            message = 'The server reported a bad request. Please check your burnup issue query for invalid JQL.'
        elif status_code == 401:  # Unauthorized
            message = 'Unauthorized'
        elif status_code == 403:  # Forbidden
            # header_name = 'X-Authentication-Denied-Reason'
            # header_value = e.response.headers[header_name]
            message = 'Please log in manually in a browser and solve the CAPTCHA before logging in here.'
        elif status_code == 404:  # Not Found
            message = 'Address not found'
        else:
            message = 'Other: ' + e.strerror

        return message


class Jira7Instance():
    def __init__(self, connector, issueQuery):
        self.setConnector(connector)
        self.setIssueQuery(issueQuery)
        self.log = logging.getLogger('Jira7Instance')

    def getIssueQuery(self):
        return self.issueQuery

    def setIssueQuery(self, issueQuery):
        self.issueQuery = issueQuery

    def getConnector(self):
        return self.connector

    def setConnector(self, connector):
        self.connector = connector

    def getScrumBoards(self):
        jsonData = self.connector.getData('rest/greenhopper/1.0/xboard/selectorData')

        boards = {}
        for board in jsonData.get('rapidViews', {}):
            if board.get('sprintSupportEnabled', None):
                boards[board['id']] = board['name']

        return boards

    def getKanbanBoards(self):
        jsonData = self.connector.getData('rest/greenhopper/1.0/xboard/selectorData')

        boards = {}
        for board in jsonData.get('rapidViews', {}):
            if board.get('sprintSupportEnabled', None):
                boards[board['id']] = board['name']

        return boards

    def getSprints(self, boardId):
        jsonData = self.connector.getData('rest/greenhopper/1.0/sprintquery/%s' % boardId, params={
            'startAt': 0,
            'maxResults': 1000
        })

        sprints = {}
        for sprint in jsonData.get('sprints', {}):
            sprints[sprint['id']] = sprint

        return sprints

    def getSprintDates(self, boardId, sprintId):
        jsonData = self.connector.getData('rest/greenhopper/1.0/rapid/charts/sprintreport', params={
            'rapidViewId': boardId,
            'sprintId': sprintId
        })

        # If no sprint found, return current time
        if not jsonData:
            self.log.info('No sprint data found, returning current time for start and end of sprint -> no chart drawn')

            import time
            import datetime
            local_tz = tzlocal.get_localzone()
            ts = time.time()
            naive = datetime.datetime.fromtimestamp(ts)
            currentTime = local_tz.localize(naive)
            return currentTime, currentTime

        endDate = jsonData['sprint']['completeDate']
        if endDate == 'None':
            endDate = jsonData['sprint']['endDate']

        localzone = tzlocal.get_localzone()

        sprintStart = localzone.localize(parser.parse(jsonData['sprint']['startDate']))
        sprintEnd = localzone.localize(parser.parse(endDate))

        return sprintStart, sprintEnd

    def getIssues(self, boardId, sprintId):
        jsonData = self.connector.getData('rest/api/2/search', params={
            'startAt': 0,
            'maxResults': 1000,
            'jql': 'issuetype = Sub-task and sprint = %s' % sprintId,
            'fields': 'timetracking,resolutiondate'
        })

        return jsonData.get('issues', {})

    def getEffortForIssues(self, boardId, issueNames):
        effortForIssues = {}
        if issueNames:
            jsonData = self.connector.getData('rest/api/2/search', params={
                'startAt': 0,
                'maxResults': 1000,
                'jql': 'issuekey in (%s)' % ','.join(issueNames),
                'fields': 'timetracking,resolutiondate'
            })

            for issue in jsonData.get('issues', {}):
                effortForIssues[issue['key']] = issue['fields']['timetracking'].get('originalEstimateSeconds', 0)

        return effortForIssues

    def getScopeChangeBurndownChart(self, rapidViewId, sprintId):
        jsonData = self.connector.getData('rest/greenhopper/1.0/rapid/charts/scopechangeburndownchart', params={
            'rapidViewId': rapidViewId,
            'sprintId': sprintId
        })

        return jsonData

    def getIssueWorklogs(self, sprintStart, sprintEnd):
        queryParts = ['(resolved >= %s or resolution = unresolved)' % TimestampUtils.timestamp_to_jqltimestamp(sprintStart),
                      '(created <= %s)' % TimestampUtils.timestamp_to_jqltimestamp(sprintEnd),
                      '(updated >= %s)' % TimestampUtils.timestamp_to_jqltimestamp(sprintStart)]

        if self.issueQuery:
            queryParts.append(self.issueQuery)

        jsonData = self.connector.getData('rest/api/2/search', params={
            'startAt': 0,
            'maxResults': 1000,
            'jql': ' and '.join(queryParts),
            'fields': 'worklog'
        })

        return jsonData.get('issues', {})
