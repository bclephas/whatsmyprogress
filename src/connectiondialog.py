from PyQt5.QtWidgets import QDialog, QLabel, QComboBox, QLineEdit, QDialogButtonBox, QGridLayout


class ConnectionDialog(QDialog):
    def __init__(self, jiraUrl, username, password):
        super().__init__()

        self.setWindowTitle('Configure connection')

        self.jiraUrlLabel = QLabel('URL')
        self.jiraUrlEdit = QLineEdit(jiraUrl)

        self.usernameLabel = QLabel('User')
        self.usernameEdit = QLineEdit(username)

        self.passwordLabel = QLabel('Password')
        self.passwordEdit = QLineEdit(password)
        self.passwordEdit.setEchoMode(QLineEdit.Password)

        buttons = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self.buttonBox = QDialogButtonBox(buttons)
        self.buttonBox.button(QDialogButtonBox.Ok).setText('Connect')

        self.gridLayout = QGridLayout(self)
        self.gridLayout.addWidget(self.jiraUrlLabel, 0, 0)
        self.gridLayout.addWidget(self.jiraUrlEdit, 0, 1)
        self.gridLayout.addWidget(self.usernameLabel, 1, 0)
        self.gridLayout.addWidget(self.usernameEdit, 1, 1)
        self.gridLayout.addWidget(self.passwordLabel, 2, 0)
        self.gridLayout.addWidget(self.passwordEdit, 2, 1)
        self.gridLayout.addWidget(self.buttonBox, 3, 0, 1, 2)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        size = self.jiraUrlEdit.sizeHint()
        self.resize(size.width() * 3, size.height() * 1)

    def getConnectionData(self):
        return (self.jiraUrlEdit.text(),
                self.usernameEdit.text(),
                self.passwordEdit.text())
