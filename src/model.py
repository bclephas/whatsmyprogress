import copy
import datetime
from dateutil import parser
import logging
import time

from PyQt5.QtCore import QObject, Qt, QThread, QTimer, pyqtSignal, pyqtSlot
import tzlocal

from utils import SortUtils, TimestampUtils


UPDATE_GRAPH_FREQUENCY = 5 * 60 * 1000


class Model(QObject):
    dataChanged = pyqtSignal()
    on_update_boards_and_sprints_ready = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.log = logging.getLogger('model')
        self.modelUpdateThreads = []

    def configure(self, jiraInstance, boardId, sprintId, availability, burnUpBudget):
        self.boardsCache = {}
        self.sprintsCache = {}

        self.adjustForHiddenWeekends = False

        self.jiraInstance = jiraInstance
        self.boardId = boardId
        self.sprintId = sprintId

        self.availability = availability
        self.burnUpBudget = burnUpBudget
        self.sprintStartDate = 0
        self.sprintEndDate = 0
        self.finalSprintScope = 0

        self.dayLabels = []
        self.dayLines = []

        self.actualBurnDownData = []
        self.idealBurnDownData = []
        self.expectedBurndownData = []

        self.actualBurnUpData = []
        self.idealBurnUpData = []
        self.projectedBurnupData = []

        self.projectedBurnupHeight = 0
        self.sprintScopeData = []
        self.zeroData = []

        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.update)

        self.modelValid = False

    def getAdjustForHiddenWeekends(self):
        return self.adjustForHiddenWeekends

    def setAdjustForHiddenWeekends(self, adjustForHiddenWeekends):
        self.adjustForHiddenWeekends = adjustForHiddenWeekends

    def getAvailability(self):
        return self.availability

    def getBurnUpBudget(self):
        return self.burnUpBudget

    def getBoardId(self):
        return self.boardId

    def setBoardId(self, boardId):
        self.boardId = boardId

    def getSprintId(self):
        return self.sprintId

    def setSprintId(self, sprintId):
        self.sprintId = sprintId

    def getAvailableBoards(self):
        return self.boardsCache

    def getAvailableSprints(self, boardId):
        return self.sprintsCache.get(boardId, {})

    def getSprintData(self, boardId, sprintId):
        sprints = self.getAvailableSprints(boardId)
        return sprints.get(sprintId, {})

    def setAvailability(self, availability):
        self.availability = availability

    def getBurnupIssueQuery(self):
        return self.jiraInstance.getIssueQuery()

    def setBurnUpBudget(self, burnUpBudget):
        self.burnUpBudget = burnUpBudget

    def getBurnDownData(self):
        return self.actualBurnDownData

    def getIdealBurnDownData(self):
        return self.idealBurnDownData

    def getBurnUpData(self):
        return self.actualBurnUpData

    def getIdealBurnUpData(self):
        return self.idealBurnUpData

    def getSprintDates(self):
        return (self.sprintStartDate, self.sprintEndDate)

    def getExpectedBurndownData(self):
        return self.expectedBurndownData

    def getProjectedBurnupHeight(self):
        return self.projectedBurnupHeight

    def getProjectedBurnupData(self):
        return self.projectedBurnupData

    def getDayLabels(self):
        return self.dayLabels

    def getDayLines(self):
        return self.dayLines

    def isValid(self):
        return self.modelValid

    def updateAvailableBoardsAndSprints(self, boardId=None):
        '''Updates all available boards and its sprints. Emits on_update_boards_and_sprints_ready signal'''
        class BoardsAndSprintsUpdateThread(QThread):
            on_boards_and_sprints_data_ready = pyqtSignal(object, object, object)

            def __init__(self, jiraInstance, boardId):
                QThread.__init__(self)
                self.jiraInstance = jiraInstance
                self.boardId = boardId

            def run(self):
                boards = self.jiraInstance.getScrumBoards()
                sprintsForBoard = self.jiraInstance.getSprints(self.boardId)

                self.on_boards_and_sprints_data_ready.emit(boards, self.boardId, sprintsForBoard)

        boardId = boardId or self.boardId
        thread = BoardsAndSprintsUpdateThread(self.jiraInstance, boardId)
        thread.on_boards_and_sprints_data_ready.connect(self._on_boards_and_sprints_data_ready)
        self.modelUpdateThreads.append(thread)
        thread.start()
        self.log.info("Requesting sprints for {}".format(boardId or "all boards"))

    def _on_boards_and_sprints_data_ready(self, boards, boardId, sprintsForBoard):
        self.log.info("Sprint data received for board %d: %s", boardId, str(sprintsForBoard.keys()))
        self.boardsCache = boards
        self.sprintsCache[boardId] = sprintsForBoard
        self.on_update_boards_and_sprints_ready.emit()

    def update(self):
        '''Update the model data. Emits dataChanged signal'''
        class UpdateModelThread(QThread):
            on_data_ready = pyqtSignal(object, object, object, object, object, object, object)

            def __init__(self, jiraInstance, boardId, sprintId, model):
                QThread.__init__(self)
                self.jiraInstance = jiraInstance
                self.boardId = boardId
                self.sprintId = sprintId
                self.model = model

            def run(self):
                sprintStartDate, sprintEndDate = self.jiraInstance.getSprintDates(self.boardId, self.sprintId)
                issues = self.jiraInstance.getIssues(self.boardId, self.sprintId)
                scopeChangeBurndownChart = self.jiraInstance.getScopeChangeBurndownChart(self.boardId, self.sprintId)
                scopeChangingIssues = self.model.getScopeChangingIssues(sprintStartDate, sprintEndDate, scopeChangeBurndownChart)

                effortForIssues = self.jiraInstance.getEffortForIssues(self.boardId, scopeChangingIssues['names'])
                issueWorklogs = self.jiraInstance.getIssueWorklogs(sprintStartDate, sprintEndDate)

                self.on_data_ready.emit(sprintStartDate, sprintEndDate, issues, scopeChangeBurndownChart, scopeChangingIssues, effortForIssues, issueWorklogs)

        thread = UpdateModelThread(self.jiraInstance, self.boardId, self.sprintId, self)
        thread.on_data_ready.connect(self._on_data_ready)
        self.modelUpdateThreads.append(thread)
        thread.start()
        self.log.info("Requesting data")

    def _on_data_ready(self, sprintStartDate, sprintEndDate, issues, scopeChangeBurndownChart, scopeChangingIssues, effortForIssues, issueWorklogs):
        self.log.info("Data received, processing ...")

        self.sprintStartDate = sprintStartDate
        self.sprintEndDate = sprintEndDate
        self.log.info("%s - %s", self.sprintStartDate, self.sprintEndDate)

        weekends = self._determineSprintWeekends(self.sprintStartDate, self.sprintEndDate)

        self.dayLabels = self._createDayLabels(self.sprintStartDate, self.sprintEndDate)
        self.dayLines = self._createDayLines(self.sprintStartDate, self.sprintEndDate)

        if not issues:
            self.log.info('No issues found to show. Connection lost or incorrect issue query?')
            self.modelValid = False
            self.dataChanged.emit()
            self.timer.start(UPDATE_GRAPH_FREQUENCY)
            return

        self.modelValid = True
        issues.sort(key=SortUtils.byResolutionDate)

        local_tz = tzlocal.get_localzone()
        ts = time.time()
        naive = datetime.datetime.fromtimestamp(ts)
        currentTime = local_tz.localize(naive)

        self.zeroData = [[self.sprintStartDate, 0], [self.sprintEndDate, 0]]

        # BurnDown
        self.scopeChangingIssues = scopeChangingIssues

        self.effortForIssues = effortForIssues

        initialSprintScope = self.getInitialScope(self.scopeChangingIssues['initial'], self.effortForIssues)
        self.sprintScopeData = self.calculateScopeChanges(self.sprintStartDate, self.sprintEndDate, self.scopeChangingIssues['changes'], self.effortForIssues)

        self.finalSprintScope = initialSprintScope + self.sprintScopeData[-1][1]

        self.actualBurnDownData = self._calculateActualBurndown(self.sprintStartDate, self.sprintEndDate, currentTime, self.finalSprintScope, issues)

        self.idealBurnDownData = self._getIdealBurndown(self.sprintStartDate, self.sprintEndDate, self.finalSprintScope)

        # BurnUp
        try:
            self.pointsPerHour = initialSprintScope / (self.availability - self.burnUpBudget)
        except ZeroDivisionError:
            self.pointsPerHour = 0

        self.actualBurnUpData = self._calculateActualBurnup(self.sprintStartDate, self.sprintEndDate, currentTime, issueWorklogs, self.burnUpBudget, self.pointsPerHour)
        projectedBurnupData = self._calculateProjectedBurnUp(self.actualBurnUpData)
        self.projectedBurnupHeight = projectedBurnupData[-1][1]
        self.expectedBurndownData = self._calculateExpectedBurndown(self.sprintStartDate, self.sprintEndDate, self.finalSprintScope, self.projectedBurnupHeight)

        self.idealBurnUpData = self._calculateIdealBurnup(self.sprintStartDate, self.sprintEndDate, self.burnUpBudget * self.pointsPerHour)

        if self.adjustForHiddenWeekends:
            self._adjustForHiddenWeekends(self.zeroData, weekends)
            self._adjustForHiddenWeekends(self.sprintScopeData, weekends)
            self._adjustForHiddenWeekends(self.idealBurnDownData, weekends)
            self._adjustForHiddenWeekends(self.actualBurnDownData, weekends)
            self._adjustForHiddenWeekends(self.dayLabels, weekends)
            self._adjustForHiddenWeekends(self.dayLines, weekends)
            self._adjustForHiddenWeekends(self.actualBurnUpData, weekends)
            self._adjustForHiddenWeekends(self.idealBurnUpData, weekends)
            self._adjustForHiddenWeekends(self.expectedBurndownData, weekends)

        self.dataChanged.emit()
        self.timer.start(UPDATE_GRAPH_FREQUENCY)

    def getZeroData(self):
        return self.zeroData

    def getRanges(self):
        min_x = TimestampUtils.timestamp_to_seconds(self.zeroData[0][0])
        max_x = TimestampUtils.timestamp_to_seconds(self.zeroData[-1][0]) + 24 * 3600
        min_y = -self.burnUpBudget * self.pointsPerHour * 1.1
        max_y = self.finalSprintScope * 1.05

        return ((min_x, min_y), (max_x, max_y))

    def _calculateActualBurndown(self, sprintStart, sprintEnd, currentTime, finalSprintScope, issues):
        totalEffortCompleted = 0
        remainingSprintEffort = finalSprintScope
        actual = [[sprintStart, remainingSprintEffort]]

        self.log.info('Calculating actual burndown')

        for value in issues:
            if not value['fields']['resolutiondate']:
                continue
            else:
                resolutionDate = parser.parse(value['fields']['resolutiondate'])
                if resolutionDate >= sprintStart and \
                   resolutionDate <= sprintEnd:
                    completedEffort = value['fields']['timetracking'].get('originalEstimateSeconds', 0) / 3600
                    totalEffortCompleted += completedEffort
                    remainingSprintEffort -= completedEffort
                    actual.append([resolutionDate, remainingSprintEffort])
                    self.log.debug('  completed %s: %.2f hours at %s' % (value['key'], completedEffort, resolutionDate))

        self.log.debug('  Overall effort completed: %.2f hours' % totalEffortCompleted)

        lastDate = currentTime if currentTime < sprintEnd else sprintEnd

        actual.append([copy.deepcopy(lastDate), remainingSprintEffort])

        return actual

    def _getIdealBurndown(self, sprintStart, sprintEnd, finalSprintScope):
        return [[copy.deepcopy(sprintStart), finalSprintScope], [copy.deepcopy(sprintEnd), 0]]

    def getIdealBurndownValueAtTimestamp(self, ts):
        self.log.info('getting ideal burndown value for %s', str(ts))

        startTimestamp = self.idealBurnDownData[0][0]
        endTimestamp = self.idealBurnDownData[-1][0]
        finalSprintScope = self.idealBurnDownData[0][1]

        try:
            burndownValue = (endTimestamp - ts) / (endTimestamp - startTimestamp) * finalSprintScope
        except ZeroDivisionError:
            burndownValue = 0

        return burndownValue

    def _calculateActualBurnup(self, sprintStart, sprintEnd, currentTime, issueWorklogs, burnupBudget, pointsPerHour):
        totalHoursIn = 0
        totalHoursOut = 0
        timeSpent = []

        self.log.info('Calculating support burnup')

        for issue in issueWorklogs:
            for worklog in issue['fields']['worklog']['worklogs']:
                created = parser.parse(worklog['created'])
                if created >= sprintStart and created <= sprintEnd:
                    timeSpent.append([created, worklog['timeSpentSeconds']])
                    totalHoursIn += worklog['timeSpentSeconds'] / 3600
                    self.log.debug('  adding %s: %.2f hours' % (issue['key'], worklog['timeSpentSeconds'] / 3600))
                else:
                    totalHoursOut += worklog['timeSpentSeconds'] / 3600
                    self.log.debug('  skipping %s: %.2f hours: creation date outside sprint boundaries' % (issue['key'], worklog['timeSpentSeconds'] / 3600))

        self.log.info('  Added a total of %.2f hours from worklogs' % totalHoursIn)
        self.log.info('  Skipped a total of %.2f hours from worklogs (creation outside sprint boundaries)' % totalHoursOut)

        timeSpent.sort(key=SortUtils.byTimestamp)

        totalTimeSpent = 0
        burnup = [[sprintStart, -burnupBudget * pointsPerHour]]
        for ts in timeSpent:
            totalTimeSpent += ts[1]
            burnup.append([ts[0], ((totalTimeSpent / 3600) - burnupBudget) * pointsPerHour])

        lastDate = currentTime if currentTime < sprintEnd else sprintEnd
        burnup.append([copy.deepcopy(lastDate), ((totalTimeSpent / 3600) - burnupBudget) * pointsPerHour])

        return burnup

    def _calculateIdealBurnup(self, sprintStart, sprintEnd, burnupBudget):
        self.log.info('Calculating ideal burnup')
        return [[copy.deepcopy(sprintStart), -burnupBudget], [copy.deepcopy(sprintEnd), 0]]

    def getInitialScope(self, initialIssues, effortForIssues):
        initialScope = 0

        self.log.info('Calculating initial sprint scope')

        for issue in initialIssues:
            effort = effortForIssues.get(issue['issueName'], 0) / 3600
            initialScope += effort
            self.log.debug("  adding %s: %.2f hours" % (issue['issueName'], effort))

        self.log.info("  Initial sprint scope is %.2f hours" % initialScope)

        return initialScope

    def calculateScopeChanges(self, sprintStart, sprintEnd, scopeChangingIssues, effortForIssues):
        scope = 0
        scopeChanges = []

        self.log.info('Calculating sprint scope changes')

        scopeChanges.append([copy.deepcopy(sprintStart), 0])

        for scopeChange in scopeChangingIssues:
            effort = effortForIssues.get(scopeChange['issueName'], 0) / 3600

            if scopeChange['added']:
                scope += effort
                self.log.debug('  added %s: %.2f hours at %s' % (scopeChange['issueName'], effort, scopeChange['timestamp']))
            else:
                scope -= effort
                self.log.debug('  removed %s: %.2f hours at %s' % (scopeChange['issueName'], effort, scopeChange['timestamp']))

            scopeChanges.append([copy.deepcopy(scopeChange['timestamp']), scope])

        self.log.info('  Overall scope change: %.2f hours' % scope)

        scopeChanges.append([copy.deepcopy(sprintEnd), scope])

        return scopeChanges

    def getScopeChangingIssues(self, sprintStart, sprintEnd, scopeChangeBurndownChart):
        initialScope = []
        scopeChanges = []

        tmpSet = set()
        issueNames = []
        alreadyDone = set()

        for timestamp, changelist in scopeChangeBurndownChart.get('changes', {}).items():
            timestamp = TimestampUtils.parseBurndownTimestamp(timestamp)

            for change in changelist:
                if 'column' in change and \
                        'done' in change['column'] and \
                        timestamp <= sprintStart:
                    alreadyDone.add(change['key'])

        for timestamp, changelist in scopeChangeBurndownChart.get('changes', {}).items():
            timestamp = TimestampUtils.parseBurndownTimestamp(timestamp)

            for change in changelist:
                # Skip parent issues
                if not scopeChangeBurndownChart['issueToParentKeys'].get(change['key'], None):
                    continue

                # Skip changes that are not sprint scope changes
                if 'added' not in change:
                    continue

                # Ignore issues that were already completed before the sprint had started
                if change['key'] in alreadyDone:
                    continue

                # Choose whether to add it to the initialScope or to the scopeChanges
                if timestamp <= sprintStart:
                    initialScope.append({'timestamp': timestamp,
                                         'added': change['added'],
                                         'issueName': change['key']})
                elif timestamp <= sprintEnd:
                    scopeChanges.append({'timestamp': timestamp,
                                         'added': change['added'],
                                         'issueName': change['key']})

                if change['key'] not in tmpSet:
                    tmpSet.add(change['key'])
                    issueNames.append(change['key'])

        initialScope.sort(key=lambda x: TimestampUtils.timestamp_to_seconds(x['timestamp']))
        scopeChanges.sort(key=lambda x: TimestampUtils.timestamp_to_seconds(x['timestamp']))

        return {'names': issueNames,
                'initial': initialScope,
                'changes': scopeChanges}

    def getSprintScopeLine(self):
        endScope = self.sprintScopeData[-1][1]
        lineData = [[value[0], endScope - value[1]] for value in self.sprintScopeData]
        return lineData

    def _createDayLabels(self, sprintStart, sprintEnd):
        labels = []
        day = sprintStart.replace(hour=12, minute=0, second=0, microsecond=0)
        if day < sprintStart:
            day += datetime.timedelta(days=1)

        while day < sprintEnd:
            # Hide weekend labels
            if day.weekday() not in [5, 6]:
                labels.append([copy.deepcopy(day), day.strftime('%a')])

            day += datetime.timedelta(days=1)

        return labels

    def _createDayLines(self, sprintStart, sprintEnd):
        lines = []
        day = sprintStart.replace(hour=0, minute=0, second=0, microsecond=0)
        if day < sprintStart:
            day += datetime.timedelta(days=1)

        while day < sprintEnd:
            # Hide weekends
            if day.weekday() not in [5, 6]:
                lines.append([copy.deepcopy(day), None])

            day += datetime.timedelta(days=1)

        return lines

    # This function is a bit different because it calculates data from a line
    # from which the weekends have already been removed. This makes it easier to
    # calculate the slope of the projected burnup.
    def _calculateProjectedBurnUp(self, actualBurnupData):
        self.log.info('Calculating projected burnup')

        burnupStart = actualBurnupData[0][0]
        burnupStartHeight = actualBurnupData[0][1]
        burnupEnd = actualBurnupData[-1][0]
        burnupEndHeight = actualBurnupData[-1][1]

        sprintStart = self.zeroData[0][0]
        sprintEnd = self.zeroData[-1][0]

        try:
            self.projectedBurnupHeight = ((burnupEndHeight - burnupStartHeight) / (burnupEnd - burnupStart).total_seconds()) * (sprintEnd - sprintStart).total_seconds() + burnupStartHeight
        except ZeroDivisionError:
            self.projectedBurnupHeight = 0

        self.log.debug('Burnup: %d', self.projectedBurnupHeight)

        return [[copy.deepcopy(burnupEnd), burnupEndHeight], [copy.deepcopy(sprintEnd), self.projectedBurnupHeight]]

    def _calculateExpectedBurndown(self, sprintStart, sprintEnd, finalSprintScope, projectedBurnupHeight):
        self.log.info('Calculating expected burndown')

        if projectedBurnupHeight < 0:
            return [[copy.deepcopy(sprintStart), finalSprintScope], [copy.deepcopy(sprintEnd), projectedBurnupHeight]]
        else:
            return []

    def _determineSprintWeekends(self, sprintStart, sprintEnd):
        endOfWeek = sprintStart.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=7 - sprintStart.weekday())
        startOfWeekend = endOfWeek - datetime.timedelta(days=2)
        weekends = []

        while startOfWeekend < sprintEnd:
            startOfNonWork = max(sprintStart, startOfWeekend)
            endOfNonWork = min(sprintEnd, endOfWeek)

            weekends.append({'start': startOfNonWork,
                             'duration': endOfNonWork - startOfNonWork})

            endOfWeek += datetime.timedelta(weeks=1)
            startOfWeekend = endOfWeek - datetime.timedelta(days=2)

        self.log.debug('Sprint weekends: %s', str(weekends))

        return weekends

    def _adjustForHiddenWeekends(self, points, weekends):
        upcomingWeekends = weekends[:]
        pointIndex = 0

        accumulatedOffset = datetime.timedelta(0)

        while pointIndex < len(points):
            nextPoint = points[pointIndex]

            if upcomingWeekends:
                nextWeekend = upcomingWeekends[0]
            else:
                nextWeekend = None

            if nextWeekend and nextPoint[0] > nextWeekend['start']:
                if nextPoint[0] > nextWeekend['start'] + nextWeekend['duration']:
                    accumulatedOffset += nextWeekend['duration']
                    upcomingWeekends.pop(0)
                else:
                    nextPoint[0] = nextWeekend['start'] - accumulatedOffset
                    pointIndex += 1
            else:
                nextPoint[0] -= accumulatedOffset
                pointIndex += 1

        return points
