Whatsmyprogress

A tool that combines a JIRA burndown and burnup graph into one.
Core graph routines based on jiraburnupanddown.
See https://github.com/Griffon26/jiraburnupanddown.

Icons: KDE Oxygen theme, LGPL 3 licensed.
       See https://techbase.kde.org/Projects/Oxygen/Licensing

Installation:

python -m venv whatsmyprogress-env
Linux: source whatsmyprogress-env\bin\activate
Windows: whatsmyprogress-env\Scripts\activate.bat
pip install -r requirements.txt
python src\whatsmyprogress.py

